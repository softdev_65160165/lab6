/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.oxgui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author nutty
 */
public class TestWritePlayer {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Player x = new Player('X');
        Player o = new Player('O');
        System.out.println(x);
        System.out.println(o);
        File file = new File("players.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(x);
        oos.writeObject(o);
        oos.close();
        fos.close();
    }
}
